<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"  omit-xml-declaration="yes" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*"/>
    <xsl:template match="log">
        <xsl:for-each select="event[origin = 'LoggingService.I3Logging.QueueStateManager' or origin='LoggingService.I3Logging.CallStatsCount']">
            <xsl:value-of select="substring(origin, 26)"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="level"/>
            <xsl:text>&#x9;</xsl:text>
            <xsl:value-of select="message"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:transform>
