<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"  omit-xml-declaration="yes" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*"/>
    <xsl:template match="log">
        <xsl:for-each select="event[origin = 'LoggingService.I3Logging.QueueStateManager' or origin='LoggingService.I3Logging.CallStatsCount' or origin='LoggingService.Global' or origin='LoggingService.I3Logging.I3LogProcessor']">
            <!-- <xsl:value&#45;of select="timestamp"/><xsl:text>&#38;#x9;</xsl:text><xsl:value&#45;of select="origin"/><xsl:text>&#38;#x9;</xsl:text><xsl:value&#45;of select="message"/><xsl:text>&#38;#xA;</xsl:text> -->
            <!-- <xsl:value&#45;of select="timestamp"/><xsl:text>&#38;#x9;</xsl:text> -->

            <!-- <xsl:variable name="short&#45;origin"> -->
            <!--     <xsl:call&#45;template name="string&#45;replace&#45;all"> -->
            <!--         <xsl:with&#45;param name="text" select="origin" /> -->
            <!--         <xsl:with&#45;param name="replace">LoggingService.</xsl:with&#45;param> -->
            <!--         <xsl:with&#45;param name="by" /> -->
            <!--     </xsl:call&#45;template> -->
            <!-- </xsl:variable> -->

            <xsl:variable name="delimeter">
                <!-- <xsl:text>&#38;#x9;</xsl:text> -->
                <xsl:text>,</xsl:text>
            </xsl:variable>

            <xsl:variable name="short-origin">
                <xsl:call-template name="select-last-part">
                    <xsl:with-param name="text" select="origin" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:call-template name="quote">
                <xsl:with-param name="text" select="$short-origin" />
            </xsl:call-template>
            <xsl:value-of select="$delimeter" />

            <xsl:call-template name="quote">
                <xsl:with-param name="text" select="level" />
            </xsl:call-template>
            <xsl:value-of select="$delimeter" />

            <xsl:call-template name="protect-message">
                <xsl:with-param name="text" select="message" />
            </xsl:call-template>
            <xsl:text>&#xA;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="select-last-part">
        <xsl:param name="text" />
        <xsl:choose>
            <xsl:when test="$text = ''" >
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, '.')">
                <xsl:call-template name="select-last-part">
                    <xsl:with-param name="text" select="substring-after($text,'.')" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = ''or not($replace)" >
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="protect-message">
        <xsl:param name="text" />

        <xsl:variable name="joined">
            <xsl:value-of select='translate($text, "&#xA;&apos;", " ")' />
        </xsl:variable>

        <xsl:variable name="quoted">
            <xsl:call-template name="quote">
                <xsl:with-param name="text" select="$joined" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:value-of select="$quoted" />
    </xsl:template>
    <xsl:template name="quote">
        <xsl:param name="text" />
        <xsl:choose>
            <xsl:when test="$text = ''">
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="concat('&quot;', $text, '&quot;')" />
            </xsl:when>
            <xsl:when test="contains($text, '&quot;')">
                <xsl:variable name="quoted">
                    <xsl:call-template name="string-replace-all">
                        <xsl:with-param name="text" select="$text" />
                        <xsl:with-param name="replace">&quot;</xsl:with-param>
                        <xsl:with-param name="by">&apos;</xsl:with-param>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="concat('&quot;', $quoted, '&quot;')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('&quot;', $text, '&quot;')" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:transform>
