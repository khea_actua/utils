@echo off

call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\Tools\vsdevcmd\ext\vcvars\vcvars140.bat"

REM GETOPS function (in case I wanna improve this): http://stackoverflow.com/a/3661082/1861346
if /I "%1" == "-d" (
	set build_type=Debug& shift
) else (
	set build_type=Release
)

REM clean and build: /t:Clean,Build

set PATH="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin;%PATH%"
set PATH="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Tools\MSVC\14.16.27023\bin\Hostx86\x64;%PATH%"

msbuild /m:4 /property:Configuration=%build_type% %1

REM vim : ffs=dos ft=bat sw=2 ts=2 :
