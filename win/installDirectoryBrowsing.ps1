function Install-DirectoryBrowsing()
{
<#
.SYNOPSIS

Install the directory browsing tags to a Web.Config file.  It can be run multiple times on the same file.

.DESCRIPTION

Install the directory browsing tags to a Web.Config file.

Another method (for site sin IIS) would be:
appcmd.exe set config "Contoso" -section:system.webServer/directoryBrowse /enabled:"True" /showFlags:"Date, Time, Size, Extension"

See https://docs.microsoft.com/en-us/iis/configuration/system.webserver/directorybrowse

.EXAMPLE

# Get list of files.  Using strings because PowershellCore can't find File objects?
$paths=(get-childitem -file -Recurse -filter 'Web.Config' -Path C:\gitrepo\EventLogManager.3 |ForEach-Object {$_.FullName})
$paths | Install-DirectoryBrowsing

#>
    [cmdletbinding()]

    PARAM
    (
        [Parameter(
            Mandatory         = $true,
            ValueFromPipeline = $true)
        ]
        [string[]]$Paths
    )

    Begin {}

    Process
    {
        Foreach ($p in $paths)
        {
            $f = Get-Item $p
            if ($f)
            {
                [xml]$wc = Get-Content $f.FullName -ErrorAction SilentlyContinue
            }
            else
            {
                Write-Host "$($f.FullName) does not exist" -ForegroundColor Red
                continue;
            }

            if ($wc)
            {
                try
                {
                    $tag_sws = addTagIfNotExists $wc.configuration 'system.webServer'
                }
                catch
                {
                    write-host "$($f.Fullname) : $_" -ForegroundColor Red
                    continue
                }
                $tag_dir = addTagIfNotExists $tag_sws 'directoryBrowse'

                $tag_dir.setAttribute('enabled', 'true');
                $tag_dir.setAttribute('showFlags', 'Date,Time,Extension,Size');

                # write-host `nFormatted XML:`r`n`n(formatXML $wc.OuterXml)

                $wc.save($f.FullName)
            }
            else
            {
                Write-Host "Could not open $($f.FullName)" -ForegroundColor Red
                continue;
            }
        }
    }

    End{}
}

function addTagIfNotExists($parent, $tag)
{
    if ($parent -eq $null)
    {
        throw "Parent provided to addTagIfNodeExists is null"
    }

    $t = $parent.SelectSingleNode($tag)
    if ($t -ne $null)
    {
        if ($t -is [array])
        {
            throw "Multiple $tag's exist, cannot continue"
        }
        elseif ($t.GetType().Name -eq 'XmlElement')
        {
            return $t
        }
    }

    # else
write-host "addTagIfNotExists: adding"
    return addElement $parent $tag
}

function addElement($parent, $tag)
{
    if ($parent.gettype().name -eq "XmlDocument") {$newtag = $parent.CreateElement($tag)}
    else {$newtag = $parent.ownerDocument.CreateElement($tag)}
    return $parent.AppendChild($newtag)
}

function formatXML([xml]$xml)
{
    $sb = New-Object System.Text.StringBuilder
    $sw = New-Object System.IO.StringWriter($sb)
    $wr = New-Object System.Xml.XmlTextWriter($sw)
    $wr.Formatting = [System.Xml.Formatting]::Indented
    $xml.Save($wr)
    return $sb.ToString()
}
