function TransformXml()
{
<#
.SYNOPSIS

Transform an XML document with a specified XSL document

.Description

Sourced from http://paregov.net/blog/19-powershell/24-xslt-processor-with-powershell

#>
    PARAM
    (
        [Parameter(Mandatory = $true)]
        [ValidateScript({Test-Path $_ -PathType 'Leaf'})]
        [String]$XmlPath,

        [Parameter(Mandatory = $true)]
        [ValidateScript({Test-Path $_ -PathType 'Leaf'})]
        [String]$XslPath,

        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [String]$Output
    )

    try
    {
        # .Load()/.Transform() are annoying in that it always wants a full
        # path, so convert the likely relative path to an object so we can
        # extract the full path
        $XslPath=Get-ChildItem -Path $XslPath
        $XmlPath=Get-ChildItem -Path $XmlPath
        if (-Not (Test-Path -Path $Output))
        {
            New-Item -Path $Output -ItemType "file"
            $Output = Get-ChildItem -Path $Output
        }

        $XslPatht = New-Object System.Xml.Xsl.XslCompiledTransform
        $XslPatht.Load($XslPath)
        $XslPatht.Transform($XmlPath, $Output)
    }
    catch
    {
        Write-Host $_.Exception -ForegroundColor Red
    }
}
