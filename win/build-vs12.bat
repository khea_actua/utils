@echo off

call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64\vcvars64.bat"

REM GETOPS function (in case I wanna improve this): http://stackoverflow.com/a/3661082/1861346
if /I "%1" == "-d" (
	set build_type=Debug& shift
) else (
	set build_type=Release
)

REM clean and build: /t:Clean,Build

msbuild /m:4 /property:Configuration=%build_type% %1