function iis_cmd()
{
	$server = "localhost"
	$siteName = "Default Web Site"
	$iis = [ADSI]"IIS://$server/W3SVC"
	$site = $iis.psbase.children | where { $_.keyType -eq "IIsWebServer" -AND
	$_.ServerComment -eq $siteName }
	$site.serverstate=3
	$site.setinfo()
}

function fix_perms()
{
	Param(
		[Parameter(Mandatory=$true)][String]
		$path
	)

	if (-Not (Test-Path $path))
	{
		write-host -ForegroundColor Red "Warning: $path doesn't exist, cannot adjust permissions"
		return;
	}
	write-host "Attempting to fix permissions for $path"

	$Acl = Get-Acl $path
	Set-Acl $path $Acl

	# Grant IIS_USER access
	$Acl = Get-Acl $path
	$Ar = New-Object system.security.accesscontrol.filesystemaccessrule("IIS_IUSRS","FullControl","Allow")
    $Acl.SetAccessRule($Ar)
}

function ReinstallELM()
{
	Param(

		[Parameter(Mandatory=$false)][String]
		$build_type = "Debug",

		[Parameter(Mandatory=$false)][String]
		$work_dir = "C:\gitrepo\EventLogManager\Installer\bin\$build_type",

		[Parameter(Mandatory=$false)][String]
		$loggingservice_install_path = "C:\inetpub\wwwroot\LoggingService\bin",

		[Parameter(Mandatory=$false)][String]
		$silverlight_install_path = "C:\inetpub\wwwroot\I3LogManager.Silverlight\bin"
	)

	$principal = new-object System.Security.Principal.WindowsPrincipal([System.Security.Principal.WindowsIdentity]::GetCurrent())
	if (-Not $principal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator))
	{
		write-host "Must run with admin"
		return
	}

	write-host "Entering $work_dir"
	cd $work_dir

	# Get a reference to the App
	write-host "Attempting to grab App object"
	$app = Get-WmiObject -Class Win32_Product -Filter "Name = 'I3Logger'"

	if ($app)
	{
		# Uninstall
		iis_cmd(2); # stopping
		$app.Uninstall()
	}
	else
	{
		Remove-Variable -Name app
		write-host "Could not uninstall App"
	}

	write-host "Installing ELM"
	# Install
	msiexec /i EventLogManager_Installer.msi /q /lv update.txt | Out-String
	iis_cmd(0); # starting

	$check_paths = ($loggingservice_install_path, $silverlight_install_path)

	foreach($p in $check_paths)
	{
		if (Test-Path $p)
		{
			ls $p
		}
		else
		{
			write-host "$p does not exist"
		}
	}

	fix_perms -Path "C:\inetpub\wwwroot\I3LogManager.Silverlight\logs"
	fix_perms -Path "C:\inetpub\wwwroot\LoggingService\logs"

	# Load the page
	# start iexplore localhost/I3LogManager.Silverlight/EventLogManager.aspx

	# Refresh the app object
	# $app = Get-WmiObject -Class Win32_Product -Filter "Name = 'I3Logger'"

	# copy C:\gitrepo\EventLogManager\I3LogManager.Silverlight.Web\bin\I3LogManager.Silverlight.Web.dll C:\inetpub\wwwroot\I3LogManager.Silverlight\bin\
}
