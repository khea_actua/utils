#!/usr/bin/env python3

from __future__ import print_function

import subprocess
import dns.resolver
import hashlib
import argparse
import configparser
import enum
import json
import sys
import os
import pwd
import re
import io

# TODO Place them into a python package such that it'll install it's
#      dependencies.  Note that it should probably be installed by root

# TODO Add an 'all' option that ignores docker options if used within docker

"""
Proxy config is hard coded to avoid injection vulnerabilities (going to
allow sudo to call this without a password if its sha can be verified)

In the future this might be replaced with a config file (that must be owned by
root or something)
"""
proxy_settings = {
    'backup_ip':   '19.12.64.237', # If we can't resolve this (not on the proxy), try this one.
    'host':        'internet.ford.com',
    'port':        83,
    'no_proxy':   'localhost,127.0.0.1,.ford.com,*.ford.com,10.1.0.3,192.168.1.26,192.168.10.100,10.1.0.1',
    'dns':
    {
        'nameservers': ['19.13.0.246', '19.69.0.246'],
        'search': ['ppp.ford.com']
    },
}

class ServicesEnum(enum.IntEnum):
    NONE          = 1 # hack for default arg
    DOCKERSERVICE = 2
    DOCKERCLIENT  = 3
    GIT           = 4
    APT           = 5

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @staticmethod
    def argparse(s):
        try:
            return ServicesEnum[s.upper()]
        except KeyError:
            return s

def demote(user_uid, user_gid):
    """Pass the function 'set_ids' to preexec_fn, rather than just calling
    setuid and setgid. This will change the ids for that subprocess only"""

    def set_ids():
        os.setgid(user_gid)
        os.setuid(user_uid)

    return set_ids

def get_demote_function(run_as_uid, service):
    """ Little helper function to get the proper pre-exec function for
    demotting permissions.  It also outputs appropriate warnings """

    if run_as_uid is None and 0 == os.getuid():
        print('Warning: toggling the %s proxy should be done by a non-root user'%service)
        preexec_fn = None
    elif run_as_uid is not None and 0 == os.getuid():
        preexec_fn = demote(run_as_uid, run_as_uid)
    elif run_as_uid is not None and 0 != os.getuid():
        print('Warning: attempting to toggling %s proxy as a different user without root access'%service)
        preexec_fn = demote(run_as_uid, run_as_uid)
    else:
        preexec_fn = None

    return preexec_fn

def toggle_git(activate=True, run_as_uid=None):
    """ Simply calls git config """

    preexec_fn = get_demote_function(run_as_uid, service='git')

    cmd = ['/usr/bin/git', 'config', '--global']
    env=None
    if run_as_uid is not None:
        env = {'HOME': pwd.getpwuid(run_as_uid).pw_dir}
    if activate:
        proxy_str='http://%s:%d'%(proxy_settings['host'], proxy_settings['port'])
        print('Activating proxy (%s) for git'%(proxy_str))
        cmd.append('http.proxy')
        cmd.append(proxy_str)
    else:
        print('Deactivating proxy for git')
        cmd.append('--unset')
        cmd.append('http.proxy')

    res = subprocess.run(cmd, env=env)


def toggle_apt(activate=True):
    fname = '/etc/apt/apt.conf.d/proxy.conf'

    if activate:
        proxy_str='http://%s:%d'%(proxy_settings['host'], proxy_settings['port'])
        print('Activating proxy (%s) for apt in %s'%(proxy_str, fname))
        contents = '''Acquire::http::Proxy "{proxy}";
Acquire::https::Proxy "{proxy}";'''.format(proxy=proxy_str)

        if 0 == os.getuid():
            with open(fname, 'w') as f:
                f.write(contents)
        else:
            print('Write the following into %s:\n%s'%(fname, contents))
    else:
        print('Deativating proxy for apt in %s'%(fname))
        if os.path.exists(fname):
            if 0 == os.getuid():
                os.remove(fname)
            else:
                print('Remove %s or comment the proxy out of it'%fname)


def toggle_docker_client(activate=True, run_as_uid=None):
    if run_as_uid is None:
        home = os.environ.get('HOME')
    else:
        home = pwd.getpwuid(run_as_uid).pw_dir

    fname=os.path.join(home, '.docker', 'config.json')

    config_file_existed = False
    if os.path.exists(fname):
        config_file_existed = True
        with open(fname) as f:
            data = json.load(f)
    else:
        data = {}

    if activate:
        proxy_str='http://%s:%d'%(proxy_settings['ip'], proxy_settings['port'])
        print('Activating proxy (%s) for docker client in %s'%(proxy_str, fname))
        data['proxies'] = {
            'default': {
                'httpProxy':  proxy_str,
                'httpsProxy': proxy_str,
                'noProxy': '{no_proxy}'.format(no_proxy=proxy_settings['no_proxy']),
            }
        }
    else:
        print('Deativating proxy for docker client in %s'%(fname))
        if 'proxies' in data:
            del data['proxies']

    if not len(data.keys()):
        os.remove(fname)
    else:
        with open(fname, 'w') as f: json.dump(data, f, indent=2)

def toggle_docker_system(activate=True):

    #
    # Modify docker service proxy conf
    # https://docs.docker.com/config/daemon/systemd/

    if 0 != os.getuid():
        print('Script must be run as root', file=sys.stderr)

    fname='/etc/systemd/system/docker.service.d/10_docker_proxy.conf'
    if not os.path.exists(os.path.dirname(fname)):
        os.mkdirs(os.path.dirname(fname))

    config = configparser.ConfigParser()
    config.read(fname)
    if activate:
        proxy_str='http://%s:%d'%(proxy_settings['host'], proxy_settings['port'])
        print('Activating proxy (%s) for docker service in %s'%(proxy_str, fname))
        if not config.has_section('Service'):
            config.add_section('Service')
        config['Service']['Environment'] = '"HTTP_PROXY={proxy}" "HTTPS_PROXY={proxy}" "NO_PROXY={no_proxy}"'.format(proxy=proxy_str, no_proxy=proxy_settings['no_proxy'])
    else:
        print('Deactivating proxy for docker service in %s'%(fname))
        if config.has_option('Service', 'environment'):
            del config['Service']['environment']

    buf = io.StringIO()
    config.write(buf)
    contents = buf.getvalue()

    # Fix the case of the keys
    contents = re.sub(r'^environment\s*=\s*', 'Environment=', contents, flags=re.MULTILINE)

    if 0 != os.getuid():
        print('Cannot write config witout root permission, write the following to %s %s'%(fname, contents))
    else:
        with open(fname, 'w') as f: f.write(contents)

    #
    # Modify daemon.json
    fname='/etc/docker/daemon.json'

    config_file_existed = False
    if os.path.exists(fname):
        config_file_existed = True
        with open(fname) as f:
            data = json.load(f)
    else:
        data = {}

    if activate:
        print('Activating DNS nameservers for docker service in %s'%(fname))
        data['dns'] = proxy_settings['dns']['nameservers']
    else:
        print('Deativating DNS nameservers for docker service in %s'%(fname))
        if 'dns' in data:
            del data['proxies']

    if not len(data.keys()):
        if 0 != os.getuid():
            print('Cannot remove config without root permission, remove the following file: %s'%fname)
        else:
            os.remove(fname)
    else:
        if 0 != os.getuid():
            print('Cannot write config without root permission, write the following to: %s\n\n%s'%(fname, json.dumps(data, indent=2)))
        else:
            with open(fname, 'w') as f: json.dump(data, f, indent=2)

    #
    # Restart docker service

    if 0 != os.getuid():
        print('''Cannot restart docker service without root permission, issue the following commands as root
%s

Then flush and restart docker with:
sudo systemctl daemon-reload
sudo systemctl restart docker
'''%(contents))
    else:
        # Probably a better way to do this:
        os.system('/usr/bin/systemctl daemon-reload')
        os.system('/usr/bin/systemctl restart docker')

def install_sudoers():
    """
    Install sudo access to this file without a password, based on
    https://unix.stackexchange.com/questions/295422/how-to-secure-a-sudo-powered-script
    """

    # Occasionally (every second time??) this doesn't work, hard-coding for now
    # script_file = os.path.abspath(sys.argv[0])
    script_file = '/home/matt/utils/bin/toggle_proxy.py'

    def sha224sum(filename):
        """
        Should be equivalent to
        openssl dgst -sha224 <file>
        """
        h  = hashlib.sha224()
        b  = bytearray(128*1024)
        mv = memoryview(b)
        with open(filename, 'rb', buffering=0) as f:
            for n in iter(lambda : f.readinto(mv), 0):
                h.update(mv[:n])
        return h.hexdigest()

    # For now just use a system call
    sha = sha224sum(script_file)

    contents = '''# Automatically generated by {script}
%{group} ALL=(ALL) NOPASSWD: sha224:{sha} {script}
'''.format(
        group='proxy',
        sha=sha,
        script=script_file
    )

    filename = '/etc/sudoers.d/toggle_proxy'
    if 0 == os.getuid():
        with open(filename, 'w') as f:
            f.write(contents)
        print('Installed %s into %s'%(script_file, filename))
    else:
        print('Add the following to %s:\n\n%s\n'%(filename, contents))

def main():
    """ Toggle proxy """

    parser = argparse.ArgumentParser(
        prog='toggle_proxy',
        description='Toggle Proxy'
    )
    group_sys = parser.add_mutually_exclusive_group()
    group_op  = parser.add_argument_group()

    group_sys.add_argument(
        '-i', '--install',
        dest='install',
        action='store_true',
        help='Install sudoers file'
    )

    group_op.add_argument(
        '-a', '--activate',
        dest='activate',
        action='store_true',
        help='Activate proxy'
    )

    group_op.add_argument(
        '-d', '--deactivate',
        dest='deactivate',
        action='store_false',
        help='Deactivate proxy'
    )

    group_op.add_argument(
        '-r', '--run-as',
        dest='run_as',
        action='store',
        default=None,
        help='Run non-system toggles as this user (not yet implemented)'
    )

    group_op.add_argument(
        dest='services',
        action='store',
        nargs='*',
        type=ServicesEnum.argparse,
        choices=tuple(ServicesEnum),
        default=ServicesEnum.NONE,
        help='List services to reload  (default: %(default)s)'
    )

    args = parser.parse_args()

    # /Load CLI parameters

    if args.install:
        install_sudoers()
        return

    # Do a lookup on the IP
    if args.activate and not 'ip' in proxy_settings:
        try:
            answers = dns.resolver.query(proxy_settings['host'])
            if len(answers):
                proxy_settings['ip'] = answers[0]
                print('Resolved %s for proxy IP'%proxy_settings['ip'])
        except:
            proxy_settings['ip'] = proxy_settings['backup_ip']
            print('Could not resolve %s, using %s'%(proxy_settings['host'], proxy_settings['backup_ip']))

    run_as_uid = None
    if args.run_as and not re.match('^\d+$', args.run_as):
        run_as_uid = pwd.getpwnam(args.run_as).pw_uid

    if args.services != ServicesEnum.NONE:
        if ServicesEnum.DOCKERSERVICE in args.services:
            toggle_docker_system(activate=args.activate)
        if ServicesEnum.DOCKERCLIENT in args.services:
            toggle_docker_client(activate=args.activate, run_as_uid=run_as_uid)
        if ServicesEnum.GIT in args.services:
            toggle_git(activate=args.activate, run_as_uid=run_as_uid)
        if ServicesEnum.APT in args.services:
            toggle_apt(activate=args.activate)

if __name__ == "__main__":
    main()

# vim: ts=4 sw=4 sts=0 expandtab ff=unix :
